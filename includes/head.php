<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Stewart Birch (Photography)</title>

	<script src="../js/vendor/custom.modernizr.js"></script>
	<script src="../js/vendor/jquery.js"></script>
	<script src="http://jquerypicture.com/js/jquery-picture-min.js"></script>

  <link rel="stylesheet" href="../css/normalize.css" />
  <link rel="stylesheet" href="../css/app.css" /> 

</head>