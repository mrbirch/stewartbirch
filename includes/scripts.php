  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? '../js/vendor/zepto' : 'js/vendor/jquery') +
  '.js><\/script>')
  </script>
   
  <script src="../js/foundation/foundation.js"></script>
	
	<script src="../js/foundation/foundation.alerts.js"></script>
	
	<script src="../js/foundation/foundation.clearing.js"></script>
	
	<script src="../js/foundation/foundation.cookie.js"></script>
	
	<script src="../js/foundation/foundation.dropdown.js"></script>
	
	<script src="../js/foundation/foundation.forms.js"></script>
	
	<script src="../js/foundation/foundation.joyride.js"></script>
	
	<script src="../js/foundation/foundation.magellan.js"></script>
	
	<script src="../js/foundation/foundation.orbit.js"></script>
	
	<script src="../js/foundation/foundation.placeholder.js"></script>
	
	<script src="../js/foundation/foundation.reveal.js"></script>
	
	<script src="../js/foundation/foundation.section.js"></script>
	
	<script src="../js/foundation/foundation.tooltips.js"></script>
	
	<script src="../js/foundation/foundation.topbar.js"></script>
	
	 <script src="../js/vendor/jquery.stickem.js"></script>
	
	
  
  <script>
    $(document).foundation();
 
    
    $('.container').stickem({
		item: '.stickem',
		container: '.stickem-container',
		stickClass: 'stickit',
		endStickClass: 'stickit-end',
		//offset: '10',
		//onStick: function(){ alert("STUCK"); },
		onUnstick: function(){ 		
		$('.stickit-end').fadeOut('fast', function() {
			
			});
			;}
		});
  </script>
  
  <script type="text/javascript">
    $(function(){
        $('figure, picture').picture();     
    });
    </script>
  
<script type="text/javascript">
  (function() {
    var config = {
      kitId: 'tfm5jjg',
      scriptTimeout: 3000
    };
    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
  })();
</script>