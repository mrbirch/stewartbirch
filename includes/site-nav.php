  <footer class="site-navigation row">
    <div class="small-12 columns">
      <hr />
      <div class="row">      
     	 <nav class="large-6 columns small-12 push-6">
     	  <ul class="inline-list footer-links ">
            <li class="categories"><a href="#">Portraits</a></li>
            <li class="categories"><a href="#">Outside</a></li>                
       
            <li><a href="#">Flickr</a></li>
            <li><a href="#">Instagram</a></li>
            <li><a href="#">Contact</a></li>          
          </ul>
        </nav>
      
        <div class="large-6 small-12 columns pull-6">
          <p>&copy; Stewart Birch 2013</p>
        </div>
     </div>
    </div>
  </footer>