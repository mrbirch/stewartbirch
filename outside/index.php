


<?php include("../includes/head.php"); ?>

<body class="outside">
<div class="site-wrapper">
		<div class="row">
			  <div class="small-6 small-centered columns"> 
  				<h1><a href="/">Stewart Birch <em>(Photography)</em></a></h1>  			
	 		 </div>

		</div><!--row-->
		
		 <div class="row">  	
  		 	<section class="photo-container large-12 small-12 columns">			
  	 			<img src="../img/outside/windows.jpg" alt="Anamur">  
       			<h1 class="photo-title">San Francisco</h1>  
            </section><!--photo-container-->
	     
	        
	        	<nav class="folio-section columns">
    				<ul class="large-block-grid-4 small-block-grid-2">
  						<li>
  							<figure>
  								<a href="itchenor.php">
  									<img src="../img/outside/thumbs/itchenor.jpg">
  									<figcaption>
  										<em>Sussex</em>
  									</figcaption>
  								</figure>
  							</a>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="kyoto.php">
  									<img src="../img/outside/thumbs/kyoto.jpg">
  									<figcaption>
  										<em>Kyoto</em>
  									</figcaption>
  								</a>
  							</figure>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="putney.php">
  									<img src="../img/outside/thumbs/putney.jpg">
  									<figcaption>
  										<em>Putney</em>
	  								</figcaption>
  								</a>
  							</figure>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="shepherds-bush.php">
  									<img src="../img/outside/thumbs/shepherds-bush.jpg">
  									<figcaption>
  										<em>Shepherds Bush</em>
  									</figcaption>
  								</a>
  							</figure>
  						</li>
  					</ul>	    	
    			</nav>
       	</div><!--row-->    	
</div><!--site-wrapper-->


 
  <!-- Footer -->
  
 		<?php include("../includes/site-nav.php"); ?>

  
 				<nav class="gallery-nav gallery-previous">
					<a href="#">Previous</a>
				</nav><!--gallery-nav-->
				
				<nav class="gallery-nav gallery-next">
					<a href="#">Next</a>
				</nav><!--gallery-nav-->
 
 
 
 	<?php include("../includes/scripts.php"); ?>
 

</body>
</html>
