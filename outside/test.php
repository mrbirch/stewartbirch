


<?php include("../includes/head.php"); ?>

<body class="outside">
<div class="site-wrapper">
		<div class="row">
			  <div class="small-6 small-centered columns"> 
  				<h1><a href="/">Stewart Birch <em>(Photography)</em></a></h1>  			
	 		 </div>

		</div><!--row-->
		
		 <div class="row">  	
  		 	<section class="photo-container large-12 small-12 columns">
  		 		<a href="outside/index.html">
  		 			<img src="../img/outside/windows.jpg" alt="Anamur">  
        			<h1 class="photo-title">San Francisco</h1>          			  
	      		 </a> 
	        </section><!--photo-container-->
	        
	        
	        
<!--
	        	<nav class="folio-section columns">
    				<ul class="large-block-grid-4 small-block-grid-2">
  						<li>
  							<figure>
  								<a href="itchenor.php">
  									<img src="../img/outside/thumbs/itchenor.jpg">
  									<figcaption>
  										Sussex
  									</figcaption>
  								</figure>
  							</a>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="kyoto.php">
  									<img src="../img/outside/thumbs/kyoto.jpg">
  									<figcaption>
  										Kyoto
  									</figcaption>
  								</a>
  							</figure>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="putney.php">
  									<img src="../img/outside/thumbs/putney.jpg">
  									<figcaption>
  										Putney
	  								</figcaption>
  								</a>
  							</figure>
  						</li>
  						
  						<li>
  							<figure>
  								<a href="">
  									<img src="../img/outside/thumbs/san-francisco.jpg">
  									<figcaption>
  										San Francisco
  									</figcaption>
  								</a>
  							</figure>
  						</li>
  					</ul>	    	
    			</nav><!--row-->
       	</div>
 	
</div><!--site-wrapper-->


 
  <!-- Footer -->
  
 		<? //php include("../includes/site-nav.php"); ?>

  
 				<nav class="gallery-nav gallery-previous">
					<a href="#">Previous</a>
				</nav><!--gallery-nav-->
				
				<nav class="gallery-nav gallery-next">
					<a href="#">Next</a>
				</nav><!--gallery-nav-->
 
 
 
 	<?php include("../includes/scripts.php"); ?>
 

</body>
</html>
